import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import { InAppBrowser, AppAvailability, Device } from 'ionic-native';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  facebook = {
    loggin: false,
    name: '',
    profilePic: '',
    email: ''
  }
  constructor(private fire: AngularFireAuth, public navCtrl: NavController) {

  }
  loginWithFacebook() {
    this.fire.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider())
      .then(res => {
        this.facebook.name = res.user.displayName;
        this.facebook.email = res.user.email;
        this.facebook.profilePic = res.user.photoURL;
        this.facebook.loggin = true;

      })

  }
  logoutOfFacebook() {
    this.fire.auth.signOut();
    this.facebook.loggin = false;
  }

  launchExternalApp(iosSchemaName: string, androidPackageName: string, appUrl: string, httpUrl: string, username: string) {
    let app: string;
    if (Device.platform === 'iOS') {
      app = iosSchemaName;
    } else if (Device.platform === 'Android') {
      let browser = new InAppBrowser(httpUrl + "/" + username, '_system');
    } else {
      let browser = new InAppBrowser(httpUrl + "/t/" + username, '_system');
      return;
    }

    AppAvailability.check(app).then(
      () => { // success callback
        let browser = new InAppBrowser(appUrl + username, '_system');
      },
      () => { // error callback
        let browser = new InAppBrowser(httpUrl + username, '_system');
      }
    );
  }

  openFacebook() {
    this.launchExternalApp('fb://', 'com.facebook.katana', 'fb://page/', 'https://www.facebook.com/messages', 'bunbouguhouse');
  }
}
